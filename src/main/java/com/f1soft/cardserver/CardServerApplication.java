package com.f1soft.cardserver;

import com.f1soft.cardserver.configuration.YamlPropertySourceFactory;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.boot.SpringApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.core.env.ConfigurableEnvironment;

@SpringBootApplication

@PropertySource(
		factory = YamlPropertySourceFactory.class,
		value =
				{
						"file:${catalina.home}/conf/rabbitmq/application-${spring.profiles.active}.yml"
				})
public class CardServerApplication extends SpringBootServletInitializer {


    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(CardServerApplication.class);
    }

	public static void main(String[] args) {
		ConfigurableApplicationContext ctx =
				SpringApplication.run(CardServerApplication.class, args);

		ConfigurableEnvironment env = ctx.getEnvironment();
		env.getPropertySources()
				.forEach(ps -> System.out.println(ps.getName() + " : " + ps.getClass()));
		System.out.println("Value of `spring.profiles.active` = " + env.getProperty("spring.profiles.active"));
		System.out.println("Value of `spring.datasource.url` = " + env.getProperty("spring.datasource.url"));
	}


}
